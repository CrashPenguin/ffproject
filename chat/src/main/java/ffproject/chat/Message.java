package ffproject.chat;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
public class Message {
	 	@Id
	    @GeneratedValue
	    private Integer id;
	    private String messageId;
	    private String body;
	    @Temporal(TemporalType.TIMESTAMP)
	    private Date created;
	    private String senderId;
	    private String receiverId;

		public String getBody() {
			return body;
		}

		public String getMessageId() {
			return messageId;
		}

		public void setMessageId(String messageId) {
			this.messageId = messageId;
		}

		public void setBody(String body) {
			this.body = body;
		}

		public Date getCreated() {
	        return created;
	    }

	    public void setCreated(Date created) {
	        this.created = created;
	    }

	    public Integer getId() {
	        return id;
	    }

	    public void setId(Integer id) {
	        this.id = id;
	    }

	    public String getSenderId() {
	        return senderId;
	    }

	    public void setSenderId(String senderId) {
	        this.senderId = senderId;
	    }

	    public String getReceiverId() {
	        return receiverId;
	    }

	    public void setReceiverId(String receiverId) {
	        this.receiverId = receiverId;
	    }

	    public String getRemoteId() {
	        return messageId;
	    }

	    public void setRemoteId(String remoteId) {
	        this.messageId = remoteId;
	    }

	    @Override
	    public boolean equals(Object o) {
	        if (this == o) return true;
	        if (o == null || getClass() != o.getClass()) return false;
	        Message message = (Message) o;
	        return Objects.equals(id, message.id);
	    }

	    @Override
	    public int hashCode() {
	        return Objects.hash(id);
	    }

		public Message() {
		}
		
		
		
		
}
