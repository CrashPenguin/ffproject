package ffproject.chat;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/message")
@RestController
public class MessageController {
	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private MessageRemoteService remoteMessageService;
	
	@Autowired
	private UserRemoteService remoteUserService;
	private List<Message> messagesList;
	@RequestMapping(value="/post", method=RequestMethod.POST)
	public Message createMessage(@RequestBody Message message) throws IllegalAccessException{
		
		if(remoteUserService.getAvailableContacts().contains(message.getReceiverId())){
		message.setCreated(new Date());
		Message serverMessage = remoteMessageService.createMessage(message);
		message.setMessageId(serverMessage.getMessageId());
		message.setRemoteId(serverMessage.getRemoteId());
		messageService.create(message);
		return message;
		} else{
			throw new IllegalAccessException("User does not exist.");
		}
	}
	
	 @RequestMapping(value="/all", method = RequestMethod.GET)
	    public List<Message> getAllMessages(){
	        return messageService.findAll();
	 }
	 
		@RequestMapping(value="/server/all", method = RequestMethod.GET)
	    public List<Message> getAllMessageFromServer(){
			return messagesList = remoteMessageService.getMessages();
}
		@RequestMapping(value="/server/newMessage", method = RequestMethod.GET)
	    public List<Message> getAllNewMessageFromServer(){
			while(!messagesList.contains(remoteMessageService.getMessages())){
				return remoteMessageService.getMessages();	 
			}
			return messagesList;
	        
}
		
}
	

