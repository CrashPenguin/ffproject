package ffproject.chat;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Service;

import ffproject.chat.AbstractRemoteService;

import java.util.List;


@Service
public class MessageRemoteService extends AbstractRemoteService {
    public List<Message> getMessages(){
        HttpHeaders headers = getDefaultHeaders();
        RequestEntity request = new RequestEntity(headers, HttpMethod.GET, prepareUrl("/api/message"));
        return restTemplate.exchange(request, new ParameterizedTypeReference<List<Message>>() {}).getBody();
    }
    public Message createMessage(Message message){
    	HttpHeaders headers = getDefaultHeaders();
    	RequestEntity requestEntity = new RequestEntity(message, headers, HttpMethod.POST, prepareUrl("/api/message"));
    	return restTemplate.exchange(requestEntity, Message.class).getBody();
    }
}