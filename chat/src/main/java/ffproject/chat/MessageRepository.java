package ffproject.chat;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

@Repository
public class MessageRepository {
	
	@PersistenceContext
	private EntityManager em;

	public void createMessage(Message message){
		em.persist(message);
	}

	public List<Message> getAll() {
	 return em
			 .createNativeQuery("Select * from message", Message.class)
			 .getResultList();
	}
	

}
