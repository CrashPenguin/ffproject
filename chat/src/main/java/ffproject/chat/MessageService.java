package ffproject.chat;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MessageService {

	@Autowired
	MessageRepository messageRepository;
	
	@Autowired
	UserRepository userRepository;
	
	public void create(Message message){
		userRepository.createIfDoesNotExist(message.getSenderId());
		userRepository.createIfDoesNotExist(message.getReceiverId());
		messageRepository.createMessage(message);
	}
	
	public List<Message> findAll(){
		return messageRepository.getAll();
	}
}
