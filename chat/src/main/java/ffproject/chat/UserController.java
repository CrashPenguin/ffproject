package ffproject.chat;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/users")
@RestController
public class UserController {
	
	@Autowired
	private UserService userService;
	@Autowired
	private UserRemoteService remoteService;
	
	@RequestMapping(value="/all", method = RequestMethod.GET)
    public Set<User> getAllUsers(){
        return userService.returnAllUsers();
 }
	@RequestMapping(value="/server/all", method = RequestMethod.GET)
    public List<String> getAllFromServer(){
        return remoteService.getAvailableContacts();
 }
	
	
}
