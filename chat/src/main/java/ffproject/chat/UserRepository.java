package ffproject.chat;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepository {
	@PersistenceContext
	private EntityManager em;

	public void createUser(String user){
		if(createIfDoesNotExist(user) == null){
		em.persist(user);
		}
	}
	
	public List<User> createIfDoesNotExist(String userId){
		return  em.createNativeQuery("Select * from users where user_Id = ?", User.class)
				.setParameter(1, userId)
				.getResultList();
	}
	
	public List<User> returnAllUsers(){
		return em.createNativeQuery("Select * from users;", User.class)
				.getResultList();
	}
}
