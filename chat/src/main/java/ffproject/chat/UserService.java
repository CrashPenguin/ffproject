package ffproject.chat;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
	
	@Autowired
	UserRepository userRepository;
	
	public String create(String userId){
		userRepository.createUser(userId);
		return userId;
	}

	public Set<User> returnAllUsers() {
		Set<User> set = new HashSet<User>();
		for(int i = 0; i<userRepository.returnAllUsers().size(); i++){
			set.add(userRepository.returnAllUsers().get(i));
		}
		return set;
	}
}
