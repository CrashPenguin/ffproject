function displayAllMessages(){
    $.get("/message/all", function(data){
    	 for(var i=0;i<data.length;i++){
             appendMessage(data[i]);
         }
    });
     
}

function addJustNewMessages(){
    $.get("/message/server/newMessage", function(data){
    	 for(var i=0;i<data.length;i++){
    			 appendMessage(data[i]);
         }
    });
     
}


function sendMessage(){
        var message = {
            senderId: $('#senderId').val(),
            receiverId: $('#receiverId').val(),
            body: $('#message').val()
        }
        $.post("/message/post", JSON.stringify(message), function(data){
        
    });

}
 
$(function(){
	$.ajaxSetup({contentType:'application/json'});
    displayAllMessages();
    $("#send").click(sendMessage);
    $("#refresh").click(addJustNewMessages);
});

function appendMessage(message){
    $('.list-group').append('<li class="list-group-item list-group-item-info">'
        + '[' + message.senderId + '] '
        + message.body
        + '<div style="float:right">' + formatDate(message.created)
        + '</div></li>');
};

function formatDate(epochDate){
return new Date(epochDate);
};